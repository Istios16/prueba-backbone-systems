<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('colonias', function (Blueprint $table) {
            $table->id();
            $table->string("codigo_postal", 5);
            $table->string('colonia');
            $table->string('tipo_colonia');
            $table->string('municipio', 50);
            $table->string('estado', 50);
            $table->string('ciudad');
            $table->integer('clave_estado');
            $table->integer('clave_colonia');
            $table->integer('clave_municipio');
            $table->integer('clave_consecutivo');
            $table->enum('zona', ['urbano', 'rural', 'semiurbano']);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('colonias');
    }
};
