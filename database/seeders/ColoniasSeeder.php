<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use App\Models\Estado;
use App\Models\Municipio;
use App\Models\Colonia;
class ColoniasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        try {
            $this->readTxt(database_path('colonias.txt'));
            $this->command->info('Registros guardados con éxito!');
        } catch(\Exception $ex) {
            $this->command->info('Ocurrio un error!');
            $this->command->info($ex->getMessage());
        }
    }

    /**
    * Lee un archivo txt y arma un array personalizado e inserta los registros a la tabla.
    *
    * @author Ismael Tirado Osuna <ismaeltiradoosuna@gmail.com>
    * @access public
    * @param string $filename nombre del archivo a abrir
    */
    public function readTxt($filename) 
    {
        $file = fopen($filename, 'r');
        $cantidad_registro = 1;
        while(!feof($file)) {
            $row = explode('|', fgets($file));
            $data = array(
                'codigo_postal'     => $row[0],
                'colonia'           => $row[1],
                'tipo_colonia'      => $row[2],
                'municipio'         => $row[3],
                'estado'            => $row[4],
                'ciudad'            => $row[5],
                'clave_estado'      => $row[7],
                'clave_colonia'     => $row[10],
                'clave_municipio'   => $row[11],
                'clave_consecutivo' => $row[12],
                'zona'              => $row[13],
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s')
            );

            Colonia::insert($data);

            $this->command->info('Registro Guardado '.$cantidad_registro);
            $cantidad_registro++;
        }

        fclose($file);
    }
}
