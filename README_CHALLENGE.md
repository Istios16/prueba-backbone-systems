# Prueba BackBone Systems

El proyecto consiste en generar un endpoint, el cual te regresará las colonias que pertenecen al código postal enviado. Para esto se tiene que llenar la base de datos con la fuente de información proporcionada (_Documento de Códigos Postales_).

## Comenzando 🚀

_¿Como se abordo el problema?_

Se realizó la descarga del documento de códigos postales del SAT en formato TXT, una ves teniendo dicho documento, se analizó dicho documento para ver que campos eran necesarios para poder replicar la funcionalidad de la api solicitada.

Una ves teniendo definidos los campos que se iban a utilizar, procedí a crear una migración para crear la tabla de **Colonias** con los siguientes campos:

* id 
* codigo_postal 
* colonia 
* tipo_colonia
* municipio
* estado
* ciudad
* clave_estado
* clave_colonia
* clave_municipio
* clave_consecutivo
* zona
* created_at
* updated_at
* deleted_at


Una ves teniendo lista mi base de datos procedí a crear un **Seeder**, en el cual, leí el archivo TXT y mediante un recorrido linea por linea y arme un arreglo de datos personalizado para poder hacer la inserción de dichos datos a la base de datos.

Una ves teniendo lista la información en mi base de datos, procedí a crear una ruta en al archivo **routes/api.php** con la sintaxis solicitada. Cree un controlador para la logica de la funcionalidad del endpoint. En dicho controlador cree una función en la cual agregue una serie de validaciones necesarias usando el **Validator** de laravel. Este regresará un mensaje de error en caso de que entre a alguna de las validaciones, esto con la finalidad de poder tener un mejor control sobre el funcionamiento del endpoint. 

En dicho endpoint se realiza una consulta hacia la tabla **Colonias** de la base de datos para obtener todas las colonias pertenecientes al código postal enviado, esto se hace mediante **Eloquent** y el uso de el modelo **Colonia** creado para interactuar con la tabla de la base de datos. Si la consulta no regresa registros por el código postal enviado, regresar un mensaje **No se encontraron registros, favor de intentar con otro código postal**. Si la consulta encuentra registros, se realiza un recorrido de los registros y se regresa la información con la estructura solicitada.

### Pre-requisitos para probar de manera local📋

Es recomendable el uso de una maquina virtual, de tu preferencia, yo les compartire la que estoy usando, **Laravel Homestead**. A continuación les comparto la documentación con los pasos a seguir:

* https://laravel.com/docs/10.x/homestead#main-content

## Despliegue 📦

* Seleccionar la carpeta donde clonaras el proyecto
* Desde consola y posicionado en la carpeta donde clonaras el proyecto ejecutar la siguiente linea de comando git clone https://gitlab.com/Istios16/prueba-backbone-systems.git
* Copiar archivo .env.example y dejarlo solamente con la extension .env
* Crear base de datos (Utf-8 general ci)
* En el archivo .env asignar tus datos de conexión a la base de datos (DB_HOST, DB_PORT, DB_DATABASE, DB_USERNAME, DB_PASSWORD)
* Dentro de la carpeta del proyecto ejecutar el comando composer install
* Dentro de la carpeta del proyecto ejecutar el comando php artisan key:generate
* Dentro de la carpeta del proyecto ejecutar el comando php artisan migrate --seed

## Construido con 🛠️

* [Laravel](https://laravel.com/) - El framework web usado

## Autor ✒️

* **Ismael Tirado Osuna** - *Web Developer | Backend Developer | PHP Developer | Laravel Developer | JavaScript Developer* 