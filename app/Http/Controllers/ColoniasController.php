<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Colonia;
use Validator;
class ColoniasController extends Controller
{
    /**
    * Obtiene las colonias con base a los parámetros de busqueda.
    *
    * @author Ismael Tirado Osuna <ismaeltiradoosuna@gmail.com>
    * @access public
    * @param string $zip_code busqueda por codigo postal
    * @return json retorna la información en un json con base a los parámetros de busqueda
    */
    public function getColonias($zip_code)
    {
        $validator = $this->validationRules($zip_code);

        if ($validator->fails()) {
            return response()->json(['type' => false, 'errores' => $validator->errors()]);
        }

        $Colonias = Colonia::where('codigo_postal', $zip_code)->get();

        if(count($Colonias) > 0) {
            $settlements = [];
            foreach ($Colonias as $key => $colonia) {
                $settlements[$key] = array(
                    "key"             => $colonia->clave_consecutivo,
                    "name"            => $this->decodeCadena($colonia->colonia),
                    "zone_type"       => strtoupper($colonia->zona),
                    "settlement_type" => array("name" => $this->decodeCadena($colonia->tipo_colonia, false))
                );
            }
    
            $row = array(
                "zip_code" => $zip_code,
                "locality" => $this->decodeCadena($Colonias[0]->ciudad),
                "federal_entity" => array(
                    "key"  => $Colonias[0]->clave_estado,
                    "name" => $this->decodeCadena($Colonias[0]->estado),
                    "code" => null
                ),
                "settlements" => $settlements,
                "municipality" => array(
                    array(
                        "key"  => $Colonias[0]->clave_municipio,
                        "name" => $this->decodeCadena($Colonias[0]->municipio)
                    )
                )
            );

            return response()->json($row);        
        } else {
            return response()->json(['type' => false, 'msg' => 'No se encontraron registros, favor de intentar con otro código postal']);
        }
        
    }

    /**
    * Valida el código postal de acuerdo a una serie de reglas.
    *
    * @author Ismael Tirado Osuna <ismaeltiradoosuna@gmail.com>
    * @access public
    * @param int $zip_code codigo postal a validar
    * @return json retorna la información de los errores con base a las reglas de validación del código postal
    */
    public function validationRules($zip_code)
    {
        $validator = Validator::make(array('zip_code' => $zip_code), [
            'zip_code'  => 'required|integer',
        ], [
            'required' => 'El campo es requerido',
            'integer'  => 'El campo debe contener solo números enteros',
        ]);

        return $validator;
    }

    /**
    * Busca los caraceteres no permitidos en una cadena de texto y los reemplaza por caracteres permitidos.
    *
    * @author Ismael Tirado Osuna <ismaeltiradoosuna@gmail.com>
    * @access public
    * @param string $cadena texto a validar y reemplazar caracteres
    * @param boolean $mayuscula boolean para validar si convertir en mayuscula la cadena
    * @return string retorna la cadena de texto, con los valores no permitidos, reemplazados por valores permitidos
    */
    public function decodeCadena($cadena, $mayuscula=true) {
        $caracteres_no_permitidos = array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä', 'é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë', 'í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î', 'ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô', 'ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü', 'ñ', 'Ñ', 'ç', 'Ç');
        $caracteres_permitidos = array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A', 'e', 'e', 'e', 'e', 'E', 'E', 'E', 'E', 'i', 'i', 'i', 'i', 'I', 'I', 'I', 'I', 'o', 'o', 'o', 'o', 'O', 'O', 'O', 'O', 'u', 'u', 'u', 'u', 'U', 'U', 'U', 'U', 'n', 'N', 'c', 'C');

        $texto = ($mayuscula) ? strtoupper(str_replace($caracteres_no_permitidos, $caracteres_permitidos, $cadena)) : str_replace($caracteres_no_permitidos, $caracteres_permitidos, $cadena);
        
        return $texto;
    }
}
